# A quick guide to root LG-H819

## checklist
* LG H819 phone
* USB cable
* Windows7 with LG driver
* Ubuntu Linux with root access
* get `git@gitlab.com:jimyang2008/lg.git` and share it between Linux and Windows7

## steps
* power off phone
* connect phone to PC with "Volume Up" pressed
* open lg_root/cmd.lnk
* get port (e.g., "COM22") from ports.bat(DIAG1 line)
```
    > ports.bat
```
* get cli to phone with "Send_Command.exe"
```
    > Send_Command.exe \\.\COM22
```
* backup system.img using "dd", and then restart
```
    # id
    # dd if=/dev/block/mmcblk0 bs=8192 skip=65536 count=548352 of=/data/media/0/system.img
    # LEAVE
```
* connect running phone to PC
* copy system.img from phone internal storage into inject_root/
* run "sudo ./autoroot.sh"
* copy rootedsystem.img back to phone internal storage
* power off phone
* connect phone to PC with "Volume Up" pressed
* open lg_root/cmd.lnk
* get port (e.g., "COM22") from ports.bat(DIAG1 line)
```
    > ports.bat
```
* get cli to phone with "Send_Command.exe"
```
    > Send_Command.exe \\.\COM22
```
* restore rootedsystem.img using "dd", and then restart
```
    # dd if=/data/media/0/rootedsystem.img bs=8192 seek=65536 count=548352 of=/dev/block/mmcblk0
    # LEAVE
```