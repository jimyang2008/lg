#!/bin/bash

chmod 0644 system.img
mount -o loop,rw -t ext4 system.img operatingtable/


mkdir operatingtable/app/SuperSU
chmod 0755 operatingtable/app/SuperSU
cp su/Superuser.apk operatingtable/app/SuperSU/SuperSU.apk
chmod 0644 operatingtable/app/SuperSU/SuperSU.apk
chcon --reference=operatingtable/bin/reboot operatingtable/app/SuperSU/SuperSU.apk


if [ ! -e operatingtable/etc/install-recovery_original.sh ]; then
 if [ -e operatingtable/etc/install-recovery.sh ]; then
  mv operatingtable/etc/install-recovery.sh operatingtable/etc/install-recovery_original.sh
 fi
fi


if [ ! -e operatingtable/bin/install-recovery_original.sh ]; then
 if [ -e operatingtable/bin/install-recovery.sh ]; then
  mv operatingtable/bin/install-recovery.sh operatingtable/bin/install-recovery_original.sh
 fi
fi
cp su/install-recovery.sh operatingtable/etc/install-recovery.sh
chmod 0755 operatingtable/etc/install-recovery.sh
chcon --reference=operatingtable/bin/toolbox operatingtable/etc/install-recovery.sh
ln -s /system/etc/install-recovery.sh operatingtable/bin/install-recovery.sh


cp su/su operatingtable/xbin/su
chmod 0755 operatingtable/xbin/su
chcon --reference=operatingtable/bin/reboot operatingtable/xbin/su


mkdir operatingtable/bin/.ext
chmod 0755 operatingtable/bin/.ext
cp su/su operatingtable/bin/.ext/.su
chmod 0755 operatingtable/bin/.ext/.su
chcon --reference=operatingtable/bin/reboot operatingtable/bin/.ext/.su


cp su/su operatingtable/xbin/daemonsu
chmod 0755 operatingtable/xbin/daemonsu
chcon --reference=operatingtable/bin/reboot operatingtable/xbin/daemonsu


cp su/su operatingtable/xbin/sugote
chmod 0755 operatingtable/xbin/sugote
chcon --reference=operatingtable/bin/app_process32 operatingtable/xbin/sugote


cp su/supolicy operatingtable/xbin/supolicy
chmod 0755 operatingtable/xbin/supolicy
chcon --reference=operatingtable/bin/reboot operatingtable/xbin/supolicy


cp su/libsupol.so operatingtable/lib64/libsupol.so
chmod 0644 operatingtable/lib64/libsupol.so
chcon --reference=operatingtable/bin/reboot operatingtable/lib64/libsupol.so


if [ ! -e operatingtable/bin/app_process64_original ]; then
 if [ -e operatingtable/bin/app_process64 ]; then
  mv operatingtable/bin/app_process64 operatingtable/bin/app_process64_original
 fi
fi
chmod 0755 operatingtable/bin/app_process64_original
chcon --reference=operatingtable/bin/app_process32 operatingtable/bin/app_process64_original


if [ -e operatingtable/bin/app_process64_original ]; then
  cp operatingtable/bin/app_process64_original operatingtable/bin/app_process_init
fi
chmod 0755 operatingtable/bin/app_process_init
chcon --reference=operatingtable/bin/reboot operatingtable/bin/app_process_init

rm operatingtable/bin/app_process
ln -s /system/xbin/daemonsu operatingtable/bin/app_process


ln -s /system/xbin/daemonsu operatingtable/bin/app_process64


if [ -e operatingtable/etc/init.d ]; then
  cp su/99SuperSUDaemon operatingtable/etc/init.d/99SuperSUDaemon
  chmod 0755 operatingtable/etc/init.d/99SuperSUDaemon
  chcon --reference=operatingtable/bin/reboot operatingtable/etc/init.d/99SuperSUDaemon
fi

# prepare GMS
rsync -rp gms/ operatingtable/priv-app/
chcon -R --reference=operatingtable/priv-app operatingtable/priv-app/GoogleServicesFramework
chcon -R --reference=operatingtable/priv-app operatingtable/priv-app/Phonesky
chcon -R --reference=operatingtable/priv-app operatingtable/priv-app/PrebuiltGmsCore

# set rw permission for external SD (for things like TiBackup)
patch operatingtable/etc/permissions/platform.xml platform.xml.diff

umount operatingtable
mv system.img rootedsystem.img
